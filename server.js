var express = require('express'),
  app = express(),
  port = process.env.PORT || 3003;

var path = require('path');

var requestjson = require('request-json');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/fmorales/collections"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var ClienteMlabRaiz;
var urlClientes = "https://api.mlab.com/api/1/databases/fmorales/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientes)
app.listen(port);

var bodyparser= require('body-parser');
app.use(bodyparser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With , Content-Type, Accept");
  next()
})
/*
var movimientosJSON=require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);
//.get .post .delete .put
app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req,res) {
  res.send("Hemos recibido su petición cambiada");
})

app.put('/', function(req,res) {
  res.send("Hemos recibido su petición put");
})


app.delete('/', function(req,res) {
  res.send("Hemos recibido su petición delete");
})


app.get('/Clientes/:idcliente', function(req,res) {
  res.send("Aquí tiene al cliente número " + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req,res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req,res) {
  res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id', function(req,res) {
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/MovimientosQuery', function(req,res) {
  console.log(req.query);
  res.send("Se recibió el query");
})

app.post('/v2/Movimientos', function(req,res) {
  var nuevo=req.body;
  nuevo.id=movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimientos dado de alta")
})


app.post('/Clientes', function(req,res){
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})
*/

app.get('/Clientes', function(req,res){
  clienteMLab.get('', function(err, resM, body){
    if (err) {
      console.log(body)
    } else {
      res.send(body)
    }
  })
})

app.get('/Movimientos', function(req,res){
  var urlMLab = urlMlabRaiz + "/Movimientos?" + apiKey;
  var clienteMLab = requestjson.createClient(urlMLab)
  clienteMLab.get('', function(err, resM, body){
    if (err) {
      console.log(body)
    } else {
      res.send(body)
    }
  })
})

app.post('/Movimientos', function(req, res) {
  /*
  "fecha":"14/08/2020",
  "importe":1500,
  "descripcion":"SPEI RECIBIDO",
  "tipo":"Transferencia interbancaria"
  */
  var fecha = req.body.fecha
  var importe = req.body.importe
  var descripcion = req.body.descripcion
  var tipo = req.body.tipo
  var urlMLab = urlMlabRaiz + "/Movimientos?" + apiKey;
  var movimientosMLab = requestjson.createClient(urlMLab)
  if (fecha!=null&&importe!=null&&descripcion!=null&&tipo!=null){
    movimientosMLab.post('', req.body, function(err, resM, body){
    res.send(body)
    if (!err) {
      res.status(200).send('Movimiento registrado')
    }
    else{
        res.status(404).send('Error al registrar movimiento')
    }
  })}
  else{
  res.status(400).send('Parametros invalidos')
  }
})


app.post('/Login', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
  console.log(query)
  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)
  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        res.status(200).send('Usuario logueado')
      } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

app.post('/Registrar', function(req, res) {
  var email = req.body.email
  var password = req.body.password
  var urlMLab = urlMlabRaiz + "/Usuarios?" + apiKey;
  var movimientosMLab = requestjson.createClient(urlMLab)
  if (email!=null&&password!=null){
    movimientosMLab.post('', req.body, function(err, resM, body){
    res.send(body)
    if (!err) {
      res.status(200).send('Movimiento registrado')
    }
    else{
        res.status(404).send('Error al registrar movimiento')
    }
  })}
  else{
  res.status(400).send('Parametros invalidos')
  }
})

app.post('/bitly/expand', function(req, res) {
  console.log("Consulta bitly")
  var bitlink_id = req.body.bitlink_id
  var url = "https://api-ssl.bitly.com/v4/expand"
  var bitlyMLab = requestjson.createClient(url)
  bitlyMLab.headers["Authorization"]="Bearer 078c31f872a8565b4e34cf0589d2fdb6c62a168d"
  bitlyMLab.headers["Content-Type"]="application/json"
  if (bitlink_id!=null){
    bitlyMLab.post('', req.body, function(err, resM, body){
      res.send(body)
      console.log("Consulta bitly correcta")
    })
  }
  else{
    res.send("Parametros incorrectos")
    console.log("Consulta bitly incorrecta")
  }
})
