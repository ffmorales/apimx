# Imagen base
FROM node:latest

# Imagen base
WORKDIR /app

# Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Pruerto que expongo
EXPOSE 3000

#Comando para ejecutar el servicio
CMD ["npm","start"]
